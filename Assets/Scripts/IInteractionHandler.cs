﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractionHandler 
{
   void Interact (IInteractionHandler whoInteracts);
      
}
